#!/usr/bin/env python3
import argparse
import dpdmgrtasks
__author__ = 'drewfle'


class Parser:

    def __init__(self, conf_dir):
        self.parser = argparse.ArgumentParser()
        self.parser.add_argument('-c', action='store', help='clones the query results from source to target')
        self.parser.add_argument('-b', action='store_true', help='stores the query results from source URIs in JSON format')
        self.args = self.parser.parse_args()
        self.tasks = dpdmgrtasks.Tasks(conf_dir)

    def parse(self):
        if vars(self.args)['c'] == 'uri':
            self.tasks.clone()
        elif vars(self.args)['b']:
            self.tasks.backup()
        else:
            print('eh... give me something! Or do a --help first, please.')
