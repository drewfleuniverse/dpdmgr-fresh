#!/usr/bin/env python3
import yaml
import json
import time
import requests
__author__ = 'drewfle'


class Tasks:

    def __init__(self, conf_dir='conf.yaml'):
        self.resource_map = []
        self.source = {}
        self.target = {}
        self.source_data = {}
        self.target_data = {}

        conf_data = self.load_conf(conf_dir)
        self.__set_resource_map(conf_data)
        self.__set_source(conf_data)
        self.__set_target(conf_data)

    @staticmethod
    def load_conf(conf_dir):
        f = open(conf_dir, "r")
        y = yaml.load(f)
        return y

    def clone(self):
        source_session = self.__login(self.source)
        self.__get_data_from_source(source_session)
        self.__save_source_to_json()
        self.__logout(self.source, source_session)

        target_session = self.__login(self.target)
        self.__get_data_from_target(target_session)
        self.__save_target_to_json()
        self.__post_data_to_target(target_session)
        self.__logout(self.target, target_session)

    def backup(self):
        source_session = self.__login(self.source)
        self.__get_data_from_source(source_session)
        self.__save_source_to_json()
        self.__logout(self.source, source_session)

        target_session = self.__login(self.target)
        self.__get_data_from_target(target_session)
        self.__save_target_to_json()
        self.__logout(self.target, target_session)

    def __save_source_to_json(self):
        with open('backups/source' + time.strftime("-%Y%m%d-%H%M%S") + '.json', 'w+') as file:
            json.dump(self.source_data, file)

    def __save_target_to_json(self):
        with open('backups/target' + time.strftime("-%Y%m%d-%H%M%S") + '.json', 'w+') as file:
            json.dump(self.source_data, file)

    def __get_data_from_source(self, session):
        for resource in self.resource_map:
            self.source_data[resource] = json.loads(session.get(self.source['url'] + '/' + resource).text)
        # print(data['calendar'][0]['title'])

    def __get_data_from_target(self, session):
        for resource in self.resource_map:
            self.target_data[resource] = json.loads(session.get(self.target['url'] + '/' + resource).text)

    def __post_data_to_target(self, session):
        for resource in self.resource_map:
            for doc in self.source_data[resource]:
                if 'id' in doc:
                    print(doc['id'])
                    doc.pop('id', None)
                r = session.post(self.target['url'] + '/' + resource, data=json.dumps(doc))
                print(r.status_code)

    def __set_resource_map(self, conf_data):
        for r in conf_data['RESOURCE_MAP']:
            self.resource_map.append(r)
        print(self.resource_map)

    def __set_source(self, conf_data):
        self.source['url'] = conf_data['SOURCE']['URL']
        self.source['login_url'] = conf_data['SOURCE']['LOGIN']
        self.source['logout_url'] = conf_data['SOURCE']['LOGOUT']
        self.source['login_data'] = {
            "username": conf_data['SOURCE']['USERNAME'],
            "password": conf_data['SOURCE']['PASSWORD']
        }

    def __set_target(self, conf_data):
        self.target['url'] = conf_data['TARGET']['URL']
        self.target['login_url'] = conf_data['TARGET']['LOGIN']
        self.target['logout_url'] = conf_data['TARGET']['LOGOUT']
        self.target['login_data'] = {
            "username": conf_data['TARGET']['USERNAME'],
            "password": conf_data['TARGET']['PASSWORD']
        }

    def __login(self, api_loc):
        s = requests.Session()
        r = s.post(api_loc['login_url'], data=api_loc['login_data'])

        if r.status_code == 200:
            print(
                'Successfully logged in ' + api_loc['login_url'] +
                ' as ' + api_loc['login_data']['username']
            )
        else:
            print('Login failed. ERROR: ' + str(r.status_code))

        return s

    def __logout(self, api_loc, session):
        r = session.post(api_loc['logout_url'])

        if r.status_code == 200:
            print('Successfully logged out ' + api_loc['login_url'])
        else:
            print('Logout failed. ERROR: ' + str(r.status_code))

