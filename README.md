## Introduction
This script assumes:
    1. The system admin would run this script on the same machine (or subnet) where hosts api instances.
    2. The api instances is running behind a proxy that doesn't use SSL, e.g. https://api.duh.man -> http://duh.man:2403.

## Prerequisites
Run `sudo apt-get install python3-dev libxml2-dev libxslt-dev` or `brew install libxml2 libxslt` if on OSX to make sure `pip` can compile the required packages.


## Configuration
This script is tested on Ubuntu 14.04.2 with Python 3.4.3 shipped with the distro. Although the development environment is isolated in Python 3 virtual environment, the detailed dependencies are listed in `requirements.txt`.

 - Change directory to `$PROJECT_HOME`
 - Create venv: `mkproject -p python3 dpdmgr`
     - Note that `python` in venv dpdmgr is already points to `python3` and `cd`ed to project folder `dpdmgr`
 - Run `git clone git@bitbucket.org:drewfleuniverse/dpdmgr-fresh.git dpdmgr`
 - Change to the project and install `pip3 install -r requirements.txt`
 - To leave the project, run `deactivate`
 - To re-enter the project, run `workon dpdmgr` and it will automatically `cd` to the project directory

Due to `virtualenvwrapper` requires the Python projects it manages to stay in `$PROJECT_HOME`, thus it may be useful to create a symlink to the `venv` project in your own project directory, e.g. `ln -s ~/PyDev/dpdmgr ~/Dev/drewfle-universe/devops-tools/dpdmgr`

## Usage

###### Clone

 - `python dpdmgr.py -c uri`

###### Backup

 - `python dpdmgr.py -b`



## References
[Intermediate and Advanced Software Carpentry](http://intermediate-and-advanced-software-carpentry.readthedocs.org/en/latest/index.html)
