#!/usr/bin/env python3
import json
import yaml
import requests
__author__ = 'drewfle'


conf = open("conf.yaml", "r")
d = yaml.load(conf)
url = 'http://localhost:2403'
data = []
s = requests.Session()
r = s.post(url + '/users/login', data={'username': 'admin', 'password': 'drewfle'})
# if status_code >= 400 || != 200, it is very wrong
print(r.status_code)

with open('json/calendar.json') as f:
    for line in f:
        data.append(json.loads(line))
        # json cannot contain "id" key, process them here
        # print(data)

for array in data:
    for doc in array:
        print('doc type: ' + str(type(doc)))
        if 'id' in doc:
            doc.pop('id', None)
            print('Got an id ...')

        # doc = {key: value for key, value in doc.items() if value != value_to_remove}
        r = s.post(url + '/calendar', data=json.dumps(doc))
        print(r.status_code)

r = s.post(url + '/users/logout')
print(r.status_code)

